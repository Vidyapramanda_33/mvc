<?php

class User extends Controller {
    public function registerView()
    {
        $data['title'] = 'register';
        $data['password_err'] = '';
        
        $this->view('auth/register', $data);
    }

    public function loginView()
    {
        $data['title'] = 'register';
        $data['message'] = '';
        
        $this->view('auth/login', $data);
    }

    public function registerProcess()
    {
        if($_POST['password'] === $_POST['verify_password']) {
            if($this->model('UserModel')->createUser($_POST) > 0) {
                header('Location: ' . BASE_URL . '/user/loginview');
                exit;
            }
        } else {
            $data['title'] = 'register';
            $data['password_err'] = 'Password must be match';
            $this->view('register/index', $data);
        }
    }
    
    public function loginProcess()
    {
        if($this->model('UserModel')->findUserByEmail($_POST['email'])) {
            $loginInUser = $this->model('UserModel')->login($_POST);
            if($loginInUser) {
                $this->model("UserModel")->session($loginInUser);
            } else {
                $data['title'] = 'Login';
                $data['message'] = 'Email Or Password Inorrect';
                $this->view('auth/login', $data);
            }
        } else {
            $data['title'] = 'Login';
            $data['message'] = 'Email Not Found';
            $this->view('auth/login', $data);
        }
    }
}